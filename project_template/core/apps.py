"""Configuração da aplicação core."""

from django.apps import AppConfig


class CoreConfig(AppConfig):
    """Configura a aplicação."""

    name = 'core'
